const express = require('express');
const fs = require('fs');
const executor = require('./usecaseExecutor.js');
const moment = require('moment');
const utils = require('./utils.js');

app = express();
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.set('port', 3011);

app.enable('trust proxy');

app.use(express.static(__dirname + '/public'));
app.use(express.urlencoded({
    extended: true
}));
app.use(express.json());

app.get('/', function (req, res) {
    res.render('home');
});


app.get('/load', function (req, res) {
    if (req.headers.accept.indexOf('application/json') >= 0) {
        res.json({ "running": _loadRunning });
    } else {
        res.render('loadgen');
    }
});

app.put('/load', function (req, res) {
    var data = req.body;
    if (data.action == "start" && !_loadRunning) {
        _loadRunning = true;
        var now = new moment();
        var msg = `Load generator starting: ${now}`;
        utils.log( msg );
        executor.startLoad();
    } else if (data.action == "stop" && _loadRunning) {
        _loadRunning = false;
    }
    res.json({ "running": _loadRunning });
});

app.get('/requests', function (req, res) {
    var requests = executor.getNoRequests();
    var startingTime = executor.startingTime();
    var status = {
        "requests": requests,
        "since": startingTime.format("dddd, MMMM Do YYYY, h:mm a") + ' (UTC)'
    }
    res.json(status);
});

// reset all services to thier original settings
// restart load 
app.post('/reset', function(req,res){
    executor.resetAllServices();
    res.send("OK");
});


app.get('/schedule', function (req, res) {
    if (req.headers.accept.indexOf('application/json') >= 0) {
        res.json({ "running": _loadRunning });
    } else {
        res.render('schedule');
    }
    
});

app.post('/schedule', function (req, res) {
    res.render('schedule');
});


//----------------------------------------------------------------------------------------------------

const package = require('./package.json');
const appName = package.name;
const appVersion = package.version;
const buildInfo = fs.readFileSync('build.txt');

app.listen(app.get('port'), '0.0.0.0', function () {
    console.log(`Starting ${appName} v${appVersion}, ${buildInfo} on port ${app.get('port')}`);
    if( typeof process.env.AUTOSTART != 'undefined' && process.env.AUTOSTART == "true" ){
        _loadRunning = true;
        var now = new moment();
        var msg = `Load generator starting: ${now}`;
        utils.log( msg );
        executor.startLoad();
    }
});

