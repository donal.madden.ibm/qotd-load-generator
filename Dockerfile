FROM node:14.16.0-alpine

RUN mkdir /app
WORKDIR /app

COPY LICENSE .
COPY README.md .
COPY package.json .
COPY build.txt .
COPY ./*.js ./
COPY views ./views
COPY usecases ./usecases
COPY public ./public

RUN npm install

EXPOSE 3011

CMD ["node", "app.js"]