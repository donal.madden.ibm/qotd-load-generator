const http = require('http');
const parseUrl = require("parse-url");
const utils = require('./utils.js');
const htmlParser = require('node-html-parser');
const fs = require('fs');
const Spline = require('cubic-spline');
const moment = require('moment');
const async = require('async');

// global so app can see
_loadRunning = false;

const xs = [ 0, 240, 420, 600, 840, 960, 1140, 1320, 1440 ];
const ys = [ 2,   2,   1,   3,   5,   4,    5,    3,    2 ];
const Schedule = new Spline(xs,ys);

var starting = new moment();
function startingTime(){
    return starting;
}
exports.startingTime = startingTime;

var noRequestsMade = 0;
function getNoRequests() {
    return noRequestsMade;
}
exports.getNoRequests = getNoRequests;

function getUrl(page,options) {
    return new Promise((resolve, reject) => {
        http.request(options, function (res) {
            utils.logLocal(`\tGET http://${options.hostname}:${options.port}${options.path}`);
            var rawData = [];
            res.on('data', (chunk) => {
                rawData.push(chunk);
            });
            res.on('end', () => {
                utils.logLocal(`\tFinished GET http://${options.hostname}:${options.port}${options.path}`);
                if (res.statusCode == 200) {
                    utils.logLocal(`\tGot resource http://${options.hostname}:${options.port}${options.path}`);
                    var result = {
                        "result": "Success",
                        "pageUrl": `http://${options.hostname}:${options.port}${options.path}`
                    }
                    if( options.headers["Accept"].indexOf("html") > 0  ) {
                        // then this is an html page, and we want to remember it
                        var buffer = Buffer.concat(rawData).toString();
                        page.html = htmlParser.parse(buffer);
                    } 
                    result.html = page.html;
                    resolve(result);
                } else {
                    reject(`Failed: ${res.statusCode}`);
                }
            });
        })
        .on('error', error => {
            reject(error);
        })
        .end();
    })
    .catch((error) => {
        utils.log(`Rejecting GET http://${options.hostname}:${options.port}${options.path}; Error: ${error}`);
    });
}

async function stepUrl(page,step) {
    var url = process.env.QOTD_WEB_HOST;
    var parsedUrl = parseUrl(url);
    var hostname = parsedUrl.resource;
    var port = 80;
    if (parsedUrl.port != null) port = parsedUrl.port;
    var path = parsedUrl.pathname;
    if (parsedUrl.search != null && parsedUrl.search.length > 0) path += "?" + parsedUrl.search;
    const options = {
        "headers": { "Accept": "text/html" },
        "method": 'GET',
        "hostname": hostname,
        "port": port,
        "path": path
    }
    try {
        var result = await getUrl(page,options);
        return result;
    } catch (error) {
        utils.log(error);
        return error;
    }
}

async function stepToAnchor(page,step) {
    try {

        var anchor = page.html.querySelector('#' + step.anchor);
        var url = anchor.getAttribute('href');
        var parsedUrl = parseUrl(url);
        var parsedPrevPage = parseUrl(page.pageUrl);

        var hostname = parsedUrl.resource;
        if (hostname == '') hostname = parsedPrevPage.resource;

        var port = 80;
        if (parsedUrl.port != null) {
            port = parsedUrl.port;
        } else if (parsedPrevPage.port != null) {
            port = parsedPrevPage.port;
        }

        var path = parsedUrl.pathname;
        if (parsedUrl.search != null && parsedUrl.search.length > 0) path += "?" + parsedUrl.search;
        const options = {
            "headers": { "Accept": "text/html" },
            "method": 'GET',
            "hostname": hostname,
            "port": port,
            "path": path
        }
        var result = await getUrl(page,options);
        return result;
    } catch (error) {
        utils.log(error);
        return error;
    }
}

async function getImage(page,step) {
    var anchor = page.html.querySelector('#' + step.img);
    var url = anchor.getAttribute('src');
    var parsedUrl = parseUrl(url);
    var parsedPrevPage = parseUrl(page.pageUrl);

    var hostname = parsedUrl.resource;
    if (hostname == '') hostname = parsedPrevPage.resource;

    var port = 80;
    if (parsedUrl.port != null) {
        port = parsedUrl.port;
    } else if (parsedPrevPage.port != null) {
        port = parsedPrevPage.port;
    }

    var path = parsedUrl.pathname;
    if (parsedUrl.search != null && parsedUrl.search.length > 0) path += "?" + parsedUrl.search;
    const options = {
        "headers": { "Accept": "image/*" },
        "method": 'GET',
        "hostname": hostname,
        "port": port,
        "path": path
    }
    try {
        var result = await getUrl(page,options);
        return result;
    } catch (error) {
        utils.log(error);
        return error;
    }
}

async function stepDelay(page,step) {
    return new Promise( (resolve,reject) => {
        setTimeout( ()=> { 
            step.status = "completed";
            resolve( page );
        },step.duration);
    });
}


function getHostUrl(step) {
    if (typeof step.url != 'undefined') return step.url;
    switch (step.service) {
        case 'web': return process.env.QOTD_WEB_HOST;
        case 'quote': return process.env.QOTD_QUOTE_HOST;
        case 'rating': return process.env.QOTD_RATING_HOST;
        case 'author': return process.env.QOTD_AUTHOR_HOST;
        case 'image': return process.env.QOTD_IMAGE_HOST;
        case 'pdf': return process.env.QOTD_PDF_HOST;
    }
    return null;
}

async function executeStep(page,step) {
    utils.logLocal(`\tStep: ${step.name}`);
    switch (step.type) {
        case 'delay': return await stepDelay(page,step);
        case 'url': return stepUrl(page,step);
        case 'url_from_anchor': return stepToAnchor(page,step);
        case 'image': return getImage(page,step);
    }
}

function done(){
    utils.logLocal('Use case completed.');
}


async function executeSteps(steps) {
    var prev_page = {
        "html": null,
        "url": null
    };
    
    async.reduce( steps, prev_page, executeStep, done );

}

async function executeUseCase(useCase) {
    executeSteps(useCase.steps);
    noRequestsMade++;
}
exports.executeUseCase = executeUseCase;

async function startUseCase(useCase) {
    await executeUseCase(useCase);
    if (_loadRunning) {
        var now = new moment();
        var hour = now.hour();  // UTC
        var minIntoDay = hour * 60 + now.minutes();
        var usecase_req_per_min = Schedule.at(minIntoDay); 
        var nominal_delay =( 60000 / usecase_req_per_min );  
        var delay = utils.genNormal(nominal_delay, nominal_delay*2, 10, 240000); 
        utils.logLocal('\tDelay: ' + delay + 'ms');
        setTimeout(() => {
            startUseCase(useCase);
        }, delay);
    }
}
exports.startUseCase = startUseCase;

function startLoad() {
    noRequestsMade = 0;
    startingTime = new moment();
    var files = fs.readdirSync('./usecases/');
    for (const file of files) {
        var data = fs.readFileSync('./usecases/' + file).toString();
        var useCase = JSON.parse(data);
        startUseCase(useCase);
    }
}
exports.startLoad = startLoad;

